TARGET = mecompat
OBJS = src/crt0.o src/main.o

INCDIR = include
CFLAGS = -Os -G0 -Wall
CXXFLAGS = $(CFLAGS) -fno-exceptions -fno-rtti
ASFLAGS = $(CFLAGS)

LIBS =
LIBDIR = lib
LDFLAGS = -nostartfiles -T src/linkfile.l -nostdlib

PSPSDK=$(shell psp-config --pspsdk-path)
include src/build.mak
